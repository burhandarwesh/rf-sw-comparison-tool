import requests
import re


# All SBTS Build Functions
def all_builds():
    sbts_Builds = requests.get("https://wft.int.net.nokia.com:8091/SBTS/api/v1/build.json?access_key=sALXuDb0hC9EMataXdN0Z7hPcateaxC9V0q6CAt1&view[items]=25&view[sorting_field]=created_at&view[sorting_direction]=DESC&view[columns[][id]]=deliverer.project.full_path&view[columns[][id]]=deliverer.title&view[columns[][id]]=version&view[columns[][id]]=branch.title&view[columns[][id]]=state&view[columns[][id]]=planned_delivery_date&view[columns[][id]]=common_links&view[columns[][id]]=compare_link&")
    sbts_Builds_Json = sbts_Builds.json()

    return sbts_Builds_Json


# All Rf Softwares Function
def rf_software():
    software_config = requests.get("https://wft.int.net.nokia.com:8091/api/v1/SBTS/CI_ENB/builds/SBTS00_ENB_9999_210524_000006/build_config?access_key=sALXuDb0hC9EMataXdN0Z7hPcateaxC9V0q6CAt1")
    rf_softwares = re.findall(r'\brf-.+\w', software_config.text, re.M | re.I)
    rf_filter = re.findall(r"\brf_.+\w", software_config.text, re.M | re.I)
    for item in rf_filter:
        rf_softwares.append(item)
    #print(len(rf_softwares))
    return sorted(rf_softwares)


# Rf Software Version Function
def rf_software_versions():

    # rf_softwares = requests.get("https://wft.int.net.nokia.com:8091/Common/api/v1/build.json?access_key=sALXuDb0hC9EMataXdN0Z7hPcateaxC9V0q6CAt1&view[items]=pi&view[sorting_field]=created_at&view[sorting_direction]=DESC&view[columns[][id]]=deliverer.project.full_path&view[columns[][id]]=deliverer.title&view[columns[][id]]=version&view[columns[][id]]=branch.title&view[columns[][id]]=state&view[columns[][id]]=planned_delivery_date&view[columns[][id]]=common_links&view[columns[][id]]=compare_link&view[view_filters_attributes[709491270082684]][column]=component&view[view_filters_attributes[709491270082684]][operation]=eq&view[view_filters_attributes[709491270082684]][value][]=rf-5GH3&")
    # rf_softwares_version = rf_softwares.json()
    software = rf_software()
    #print(software)
    for items in software:
        rf_softwares = requests.get(f"https://wft.int.net.nokia.com:8091/Common/api/v1/build.json?access_key=sALXuDb0hC9EMataXdN0Z7hPcateaxC9V0q6CAt1&view[items]=25&view[sorting_field]=created_at&view[sorting_direction]=DESC&view[columns[][id]]=deliverer.project.full_path&view[columns[][id]]=deliverer.title&view[columns[][id]]=version&view[columns[][id]]=branch.title&view[columns[][id]]=state&view[columns[][id]]=planned_delivery_date&view[columns[][id]]=common_links&view[columns[][id]]=compare_link&view[view_filters_attributes[709491270082684]][column]=component&view[view_filters_attributes[709491270082684]][operation]=eq&view[view_filters_attributes[709491270082684]][value][]={items}&")
        rf_softwares_version = rf_softwares.json()
        # for i in rf_softwares_version['items']:
        #     print(f"{items}:{i.get('version')}")
        return rf_softwares_version

def list_Radio_type():
    radioDescription = ["AZQG,AZQH,AZQL", "N/A (LTE Radio)", "N/A (LTE Radio)", "5G/LTE FDD Radios", "AHFIE (LTE)",
                        "AEQN eCPRI", "AENB, AEQM",
                        "AHPF, AWHHD, AWHHF, AWHHG,AWHHH,AWHHI,AWHQE,AWHQF,AWHQG,AWHQM,AWHQV", "RF_TLDA_plugin",
                        "AEQN CPRI",
                        "AZQC,AZQH,AZQL", "AAHF,AAHJ", "AAHF,AAHJ", "AEUA,AEUF,AEWA,AEWF", "AEUB,AEUD,AEWB,AEWD",
                        "AHQK", "AEHA,AEQA,AEQD,AETB,AETF",
                        "AAHF,AAHJ", "AWEUA,AWEUB,AWEUC,AWEUD,AWEWA,AWEWB,AWGUC,AWGUD", "", "AEHB,AEHC(Chile)",
                        "AQQE,AQQK,AQQL", "SCORPIO", "N/A(LTE Radio)", "AEQH,AEQJ"]

    return radioDescription

if __name__ == '__main__':
    rf_software()