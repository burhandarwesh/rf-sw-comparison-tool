from flask import Flask, render_template, jsonify, request
import requests
import api_data

app = Flask(__name__)

stars = "***********************************************************"

# Software Comparison
@app.route('/softwareComparison/', methods = ["GET", "POST"])
def softwareComparison():

        rF5GH3 = requests.get(f"https://wft.int.net.nokia.com:8091/api/v1/Common/rf-CHB0/builds/CHB01.05.R11.json?items[]=peg_revisions&access_key=sALXuDb0hC9EMataXdN0Z7hPcateaxC9V0q6CAt1")

        readJson3 = rF5GH3.json()
        rF5GH4 = requests.get(f"https://wft.int.net.nokia.com:8091/api/v1/Common/rf-CHB0/builds/CHB01.05.R17.json?items[]=peg_revisions&access_key=sALXuDb0hC9EMataXdN0Z7hPcateaxC9V0q6CAt1")
        readJson4 = rF5GH4.json()
        # print(readJson3)
      #stars + readJson4["baseline"], readJson4["peg_revisions"]
        jsonLoad = readJson3.get("peg_revisions")
        jsonLoad2 = readJson4.get("peg_revisions")
    #print(jsonLoad)
        idSW1 = [item['id'] for item in jsonLoad]
        peg_revisionSW1 = [item['peg_revision'] for item in jsonLoad]
        scSW1 = [item['sc'] for item in jsonLoad]
        projectSW1 = [item['project'] for item in jsonLoad]

        idSW2 = [item2['id'] for item2 in jsonLoad2]
        peg_revisionSW2 = [item2['peg_revision'] for item2 in jsonLoad2]
        scSW2 = [item2['sc'] for item2 in jsonLoad2]
        projectSW2 = [item2['project'] for item2 in jsonLoad2]


        a = set(scSW1)
        #print(len(a))
        b = set(scSW2)
        sw1len = len(a)
        sw2len = len(b)
        # print(len(b))
        # print(len(scSW1))
        # print(len(scSW2))
        # print(len(peg_revisionSW1))
        # print(len(peg_revisionSW2))

    # Common Component of from both software versions
        common_PegRevision_SW1 = [peg_revisionSW1[i] for i in range(len(scSW1)) if peg_revisionSW1[i] in peg_revisionSW1
                                  and peg_revisionSW1[i] in peg_revisionSW2]
        commonComponent = [scSW1[item] for item in range(sw1len) if sorted(a.intersection(b),key=lambda v: v.upper()) and peg_revisionSW1[item] in common_PegRevision_SW1] #sorted(a.intersection(b), key=lambda v: v.upper())

        # print(len(commonComponent))
        commonLen = len(commonComponent)
        #print(commonLen)

    # Different Component of software versions 1 sorted(var, key=lambda v: v.upper())
        #different_Component_SW1 = sorted(a.difference(b), key=lambda v: v.upper())
        different_Component_SW1 = [scSW1[item] for item in range(sw1len) if scSW1[item] in scSW1
                                   and peg_revisionSW1[item] not in common_PegRevision_SW1]
        diffLen = len(different_Component_SW1)
        #print(different_Component_SW1)
        print(diffLen)

    # Different Component of software versions 2
        different_Component_SW2 = [scSW2[item] for item in range(sw2len) if scSW2[item] in scSW1
                                   and peg_revisionSW2[item] not in common_PegRevision_SW1]
        diffLenSW2 = len(different_Component_SW2)
        print(different_Component_SW2)
        #print(diffLenSW2)

    # IDs and Peg Revisions of both Software Versions
        #common_PegRevision_SW1 = [peg_revisionSW1[i] for i in range(len(scSW1)) if peg_revisionSW1[i] in peg_revisionSW1
                              #and peg_revisionSW1[i] in peg_revisionSW2]
        #common_Component_SW1 = [scSW1[i] for i in range(len(scSW1)) if scSW1[i] in scSW1 and scSW1[i] in scSW2]
        #print(commonComponent)
        #print(len(common_Component_SW1))

        #print(common_PegRevision_SW1)
        common_ID_SW1 = [idSW1[i] for i in range(len(scSW1)) if peg_revisionSW1[i] in peg_revisionSW1
                     and scSW1[i] in commonComponent]
        different_pegRevision_SW1 =[peg_revisionSW1[i] for i in range(len(scSW1)) if peg_revisionSW1[i] in peg_revisionSW1
                                and peg_revisionSW1[i] not in peg_revisionSW2]
        #print(different_pegRevision_SW1)
        different_ID_SW1 = [idSW1[i] for i in range(len(scSW1)) if peg_revisionSW1[i] in peg_revisionSW1 and scSW1[i]
                        not in commonComponent]


        common_ID_SW2 = [idSW2[i] for i in range(len(scSW2)) if peg_revisionSW2[i] in peg_revisionSW2
                     and scSW2[i] in commonComponent]
        different_pegRevision_SW2 = [peg_revisionSW2[i] for i in range(len(scSW2)) if peg_revisionSW2[i] in peg_revisionSW2
                                 and peg_revisionSW2[i] not in peg_revisionSW1]
        df_pegLen = len(different_pegRevision_SW2)
        print(different_pegRevision_SW2)
        #print(df_pegLen)
        different_ID_SW2 = [idSW2[i] for i in range(len(scSW2)) if peg_revisionSW2[i] in peg_revisionSW2 and scSW2[i]
                        not in commonComponent]
        # for item in range(sw1len):
        #         if sorted(a.difference(b), key=lambda v: v.upper()) and peg_revisionSW1[item] in different_pegRevision_SW1:
        #                 print(peg_revisionSW1[item], scSW1[item])





        return render_template('newhtml.html',readJson3=readJson3,readJson4=readJson4,commonLen=commonLen, commonComponent=commonComponent,
                           common_PegRevision_SW1=common_PegRevision_SW1, project=projectSW1, common_ID_SW1=common_ID_SW1 ,
                           diffLen=diffLen, different_Component_SW1=different_Component_SW1,diffLenSW2=diffLenSW2,
                           different_Component_SW2=different_Component_SW2,different_ID_SW1=different_ID_SW1,
                           projectSW2=projectSW2, different_pegRevision_SW1=different_pegRevision_SW1,
                           common_ID_SW2=common_ID_SW2,different_pegRevision_SW2=different_pegRevision_SW2,
                           different_ID_SW2=different_ID_SW2)


@app.route('/softwareList/')
def softwareList():

    sbts_Builds = api_data.all_builds()
    sbts_updated = [item.get('version') for item in sbts_Builds['items']]
    rf_softwares = api_data.rf_software()
    rf_SW_versions = api_data.rf_software_versions()
    rfLen =len(rf_softwares)
    radioList = api_data.list_Radio_type()
    return render_template("xyz.html",sbts_Builds = sbts_Builds, rf_softwares= rf_softwares,
                           rf_SW_versions=rf_SW_versions,rfLen=rfLen, radioList=radioList,sbts_updated=sbts_updated)


# Home page

if __name__ == '__main__':
   app.run(debug = True)