from flask import Flask, render_template, jsonify, request
import requests
import api_data

import scorpio_AZHL_Trunk

app = Flask(__name__)

stars = "***********************************************************"

# Software Comparison
@app.route('/softwareComparison/', methods = ["GET", "POST"])
def softwareComparison():

    if request.method == 'POST':
        radioComponent = request.form['radioComponent']
        software_1 = request.form['Software1'].strip()
        software_2 = request.form['Software2'].strip()

        rF5GH3 = requests.get(f"https://wft.int.net.nokia.com:8091/api/v1/Common/{radioComponent.upper()}/builds/{software_1}.json?items[]=peg_revisions&access_key=sALXuDb0hC9EMataXdN0Z7hPcateaxC9V0q6CAt1")
        readJson3 = rF5GH3.json()
        rF5GH4 = requests.get(f"https://wft.int.net.nokia.com:8091/api/v1/Common/{radioComponent.upper()}/builds/{software_2}.json?items[]=peg_revisions&access_key=sALXuDb0hC9EMataXdN0Z7hPcateaxC9V0q6CAt1")
        readJson4 = rF5GH4.json()

        jsonLoad = readJson3.get("peg_revisions")
        jsonLoad2 = readJson4.get("peg_revisions")
    #print(jsonLoad2)
    #print(jsonLoad)
        idSW1 = [item['id'] for item in jsonLoad]
        peg_revisionSW1 = [item['peg_revision'] for item in jsonLoad]
        scSW1 = [item['sc'] for item in jsonLoad]
        projectSW1 = [item['project'] for item in jsonLoad]

        idSW2 = [item2['id'] for item2 in jsonLoad2]
        peg_revisionSW2 = [item2['peg_revision'] for item2 in jsonLoad2]
        scSW2 = [item2['sc'] for item2 in jsonLoad2]
        projectSW2 = [item2['project'] for item2 in jsonLoad2]


        a = set(scSW1)
        #print(len(a))
        b = set(scSW2)
        # print(len(b))

    # Common Component of from both software versions
        common_PegRevision_SW1 = [peg_revisionSW1[i] for i in range(len(scSW1)) if peg_revisionSW1[i] in peg_revisionSW2
                                  and scSW1[i] in scSW2]
        #print(common_PegRevision_SW1)
        # commonComponent = [scSW1[item] for item in range(len(scSW1)) if
        #                    sorted(a.intersection(b), key=lambda v: v.upper()) and peg_revisionSW1[
        #                        item] in common_PegRevision_SW1]
        commonComponent = [scSW1[item] for item in range(len(scSW1)) if scSW1[item] in scSW2 and peg_revisionSW1[
                               item] in common_PegRevision_SW1]
        #common = [peg_revisionSW1[i] for i in range(len(scSW1)) if]
        #print(common)
        # common_PegRevision_SW2 = [peg_revisionSW2[i] for i in range(len(scSW1)) if peg_revisionSW1[i] in peg_revisionSW1
        #                           and peg_revisionSW1[i] in peg_revisionSW2]
        # commonComponent_SW2 = [scSW2[item] for item in range(len(scSW1)) if
        #                    sorted(a.intersection(b), key=lambda v: v.upper()) and peg_revisionSW1[
        #                        item] in common_PegRevision_SW1]

        commonLen = len(commonComponent)

    # Different Component of software versions 1
        different_Component_SW1 = [scSW1[item] for item in range(len(scSW1)) if scSW1[item] in scSW1
                                   and scSW1[item] not in commonComponent]
        diffLen = len(different_Component_SW1)
        #print(diffLen)

    # Different Component of software versions 2
        different_Component_SW2 = [scSW2[i] for i in range(len(scSW2)) if peg_revisionSW2[i] in peg_revisionSW2
                                 and scSW2[i] not in commonComponent]
        diffLenSW2 = len(different_Component_SW2)

    # IDs and Peg Revisions of both Software Versions
        #common_PegRevision_SW1 = [peg_revisionSW1[i] for i in range(len(scSW1)) if peg_revisionSW1[i] in peg_revisionSW1
                                  #and peg_revisionSW1[i] in peg_revisionSW2]
        common_ID_SW1 = [idSW1[i] for i in range(len(scSW1)) if peg_revisionSW1[i] in peg_revisionSW1
                     and scSW1[i] in commonComponent]
        different_pegRevision_SW1 =[peg_revisionSW1[i] for i in range(len(scSW1)) if peg_revisionSW1[i] in peg_revisionSW1
                                and scSW1[i] not in commonComponent]
        #print(len(different_pegRevision_SW1))
        different_ID_SW1 = [idSW1[i] for i in range(len(scSW1)) if peg_revisionSW1[i] in peg_revisionSW1 and scSW1[i]
                        not in commonComponent]


        common_ID_SW2 = [idSW2[i] for i in range(len(scSW2)) if peg_revisionSW2[i] in peg_revisionSW2
                     and scSW2[i] in commonComponent]
        different_pegRevision_SW2 = [peg_revisionSW2[i] for i in range(len(scSW2)) if peg_revisionSW2[i] in peg_revisionSW2
                                 and scSW2[i] not in commonComponent]
        #print(len(different_pegRevision_SW2))
        different_ID_SW2 = [idSW2[i] for i in range(len(scSW2)) if peg_revisionSW2[i] in peg_revisionSW2 and scSW2[i]
                        not in commonComponent]




        return render_template('index.html',readJson3=readJson3,readJson4=readJson4,commonLen=commonLen, commonComponent=commonComponent,
                       common_PegRevision_SW1=common_PegRevision_SW1, project=projectSW1, common_ID_SW1=common_ID_SW1 ,
                       diffLen=diffLen, different_Component_SW1=different_Component_SW1,diffLenSW2=diffLenSW2,
                       different_Component_SW2=different_Component_SW2,different_ID_SW1=different_ID_SW1,
                       projectSW2=projectSW2, different_pegRevision_SW1=different_pegRevision_SW1,
                       common_ID_SW2=common_ID_SW2,different_pegRevision_SW2=different_pegRevision_SW2,
                       different_ID_SW2=different_ID_SW2) #common_PegRevision_SW2=common_PegRevision_SW2,commonComponent_SW2=commonComponent_SW2
        # else:
        #     if radioComponent not in readJson3 and software_1 not in readJson3 or radioComponent not in readJson4 and software_2 not in readJson4:
        #         return jsonify(f"error: RF component{radioComponent} and both SW Versions {software_1} and {software_2} is not valid, These are not Scorpio radio"), 403
        #     elif radioComponent not in readJson3 and radioComponent not in readJson4:
        #         return jsonify(f"error: RF Component {radioComponent} is not valid for comparison, This is not Scorpio radio"),
        #     elif software_1 not in readJson3:
        #         return jsonify(f"error: Software version {software_1} is not valid for comparison, This is not Scorpio radio"), 403
        #     elif software_2 not in readJson4:
        #         return jsonify(f"error: Software version {software_2} is not valid for comparison, This is not Scorpio radio"),

# Home page
@app.route('/')
def homePage():
    return render_template("homePage.html")

#Software List for dropdown menu
@app.route('/softwareList/')
def softwareList():

    sbts_Builds = api_data.all_builds()
    rf_softwares = api_data.rf_software()
    rf_SW_versions = api_data.rf_software_versions()
    rfLen =len(rf_softwares)
    radioList = api_data.list_Radio_type()
    return render_template("dropDown.html",sbts_Builds = sbts_Builds, rf_softwares= rf_softwares,
                           rf_SW_versions=rf_SW_versions,rfLen=rfLen, radioList=radioList)

@app.route('/chartComparisonMenu/', methods = ["GET", "POST"])
def chartComparison():
    df = scorpio_AZHL_Trunk.scorpio_table_data()
    date = df['Date']
    sbts_builds = df['BTS Build']
    rfSW_Version = df['RFSW (release index)']
    build_No = df['Build #']




    length = len(sbts_builds)
    return render_template('chartMenu.html', sbts_builds= sbts_builds,length=length,date=date,rfSW_Version=rfSW_Version,
                           build_No = build_No)

@app.route('/graphComparison/', methods = ["GET", "POST"])
def graph_comparison():
    if request.method == 'POST':
        #radioComponent = request.form['radioComponent']
        software_1 = request.form['Software1'].strip()
        software_2 = request.form['Software2'].strip()

        #print(software_1)
        df = scorpio_AZHL_Trunk.scorpio_trunK_data()

        # data = pd.read_excel(r'Z:\5G_3\Oulu\buru\Scorpio_AZHL_Trunk_Build_Validation_Diary.xlsx')
        # df = pd.DataFrame(data)
        # df.drop(df.iloc[:, 14:], inplace = True, axis = 1)
        # pd.set_option('display.max_columns',None)
        rfSoftware_version = [item for item in df['RFSW (release index)'].str.strip()]
        if software_1 in rfSoftware_version and software_2 in rfSoftware_version:
            label = [item for item in df.columns]
            values = [item for item in df.values.tolist()]
            #print(values)
            legend = 'Software Comparison Graph'
            sw_1 = df[df['RFSW (release index)'].str.match(software_1)].values.tolist()
            sw_2 = df[df['RFSW (release index)'].str.match(software_2)].values.tolist()

            #sw_len = len(sw_1)

            #print(sw_1)
            return render_template("barChart.html", label=label, legend=legend, values=values, software_1=software_1,
                                   software_2=software_2, sw_1=sw_1, sw_2=sw_2)
        else:
            if software_1 not in rfSoftware_version and software_2 not in rfSoftware_version:
                return jsonify(f"error: Both software version {software_1} and {software_2} is not valid, These versions are not belong to Scorpio radio"), 403
            elif software_1 not in rfSoftware_version:
                return jsonify(f"error: Software version {software_1} is not valid for comparison, This version is not belong to Scorpio radio"), 403
            elif software_2 not in rfSoftware_version:
                return jsonify(f"error: Software version {software_2} is not valid for comparison, This version is not belong to Scorpio radio"), 403


        # if df.loc[df['RFSW (release index)'] == software_1 & df['RFSW (release index)'] == software_2]:
        #     print("yes they are here")
        # else:
        #     return jsonify({"error": "Incorrect Software Values"}), 403



if __name__ == '__main__':
   app.run(debug = True, host="0.0.0.0",port=5000)