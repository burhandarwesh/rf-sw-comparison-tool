import pandas as pd



def scorpio_trunK_data():
    data = pd.read_csv('Scorpio_AZHL_Trunk_Build_Validation_Diary.csv')
    df = pd.DataFrame(data).iloc[:,3:16]
    #df.drop(df.iloc[:, 15:], inplace=True, axis=1)
    pd.set_option('display.max_columns', None)
    #print(df)
    return df

def scorpio_table_data():
    data = pd.read_csv('Scorpio_AZHL_Trunk_Build_Validation_Diary.csv')
    df = pd.DataFrame(data)
    df.drop(df.iloc[:, 4:], inplace=True, axis=1)
    return df

def true_false():
    data = pd.read_csv("Scorpio_AZHL_Trunk_Build_Validation_Diary.csv")
    df = pd.DataFrame(data).iloc[:,3:16]
    pd.set_option('display.max_columns',None)
    #df.drop(df.iloc[:,3:16], inplace=True, axis=1)


