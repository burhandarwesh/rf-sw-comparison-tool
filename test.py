import unittest
from data_comparison import *
import requests
import requests


class MyTestCase(unittest.TestCase):
    def test_index(self):
        tester = app.test_client(self)
        response = tester.get('/', content_type = 'html/text')
        self.assertEqual(response.status_code, 200)

    def test_Homepage(self):
        tester = app.test_client(self)
        response = tester.get('/', content_type='html/text')
        self.assertTrue(b'RF Software Comparison' in response.data)

    def test_software_compare(self):
        tester = app.test_client(self)
        response = tester.post('/softwareList', data=dict(software1='ERM61.05.R10 ',software2= 'ERM61.06.R02')
                               ,follow_redirects=True)
        self.assertIn(b'', response.data)



if __name__ == '__main__':
    unittest.main()
