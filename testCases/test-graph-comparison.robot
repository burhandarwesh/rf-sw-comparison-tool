*** Settings ***
Library  SeleniumLibrary

Suite Setup     Open BROWSER    ${LOGIN URL}    ${BROWSER}
Suite Teardown  Close All Browsers
Library  SeleniumLibrary
#*** Keywords ***

*** Variables ***
${LOGIN URL}      http://127.0.0.1:5000
${BROWSER}        headlesschrome
${jenkins_sofware_version}    "nonexistent"
${jenkins_comparewith}    "nonexistent"

*** Test Cases ***
Graph Comparison TestCase

    #OPEN BROWSER    ${LOGIN URL}    ${BROWSER}
    maximize browser window
    click link    xpath:/html/body/div[2]/a[2]     #graph button
    sleep    5
    input text    id:Software1     ${jenkins_sofware_version}
    #log to console   # ${jenkins_sofware_version}
    input text    name:Software2   ${jenkins_comparewith}
    #log to console    ${jenkins_comparewith}
    click element    xpath:/html/body/div[2]/form/p/input
    ${compare_graph}=   get text    id:chart      #xpath:/html/body/canvas
    ${comments} =    get text     xpath:/html/body/table

    log to console    \n"Taking screenshot"
    capture element screenshot    xpath:/html/body/canvas    graphComparison.png
    execute javascript    window.scroll(0,100)
    capture element screenshot    xpath:/html/body/table    comparisonStatus.png
    log to console    \n"Screenshot saved"
    log to console    \n"Fetching Software 1 data....."
    log to console    ${compare_graph} \n"Fetching compeleted..."
    log to console   \n"Fetching Software 2 data"
    log to console    ${comments} \n"Fetching compeleted..."




