*** Settings ***
Library  SeleniumLibrary
Suite Setup     Open BROWSER    ${LOGIN URL}    ${BROWSER}
Suite Teardown  Close All Browsers
#*** Keywords ***

*** Variables ***
${LOGIN URL}      http://127.0.0.1:5000
${BROWSER}        headlesschrome
${jenkins_sofware_version}    "nonexistent"
${jenkins_comparewith}    "nonexistent"

*** Test Cases ***
RF Test Case
    maximize browser window
    #OPEN BROWSER    ${LOGIN URL}    ${BROWSER}
    click link    xpath:/html/body/div[2]/a[1]    #table Button
    sleep    5
    #click link    xpath:/html/body/div[2]/a[2]     #graph button
    input text  id:Software1    ${jenkins_sofware_version}
    log to console    ${jenkins_sofware_version}
    input text  id:Software2   ${jenkins_comparewith}
    log to console    ${jenkins_comparewith}
    click element    xpath:/html/body/div[3]/form/p/input
    ${sw1_data}=    get text     xpath:/html/body/div[2]/div[1]/table
    ${sw2_data} =   get text     xpath:/html/body/div[2]/div[2]/table
    log to console    \n"Taking screenshot"
    log to console    \n"Screenshot saved"
    log to console    \n"Fetching Software 1 data....."
    log to console    ${sw1_data} \n"Fetching compeleted..."
    log to console   \n"Fetching Software 2 data"
    log to console    ${sw2_data} \n"Fetching compeleted..."


